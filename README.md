# CI&T

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.6.

## Installing

Run `npm install` to install all dependencies.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
