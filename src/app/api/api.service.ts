import {Observable} from 'rxjs';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService extends HttpClient {

  constructor(handler: HttpHandler) {
    super(handler);
  }

  get(endpoint: string, options: any = { responseType: 'text' }): Observable<any> {
    return super.get(endpoint, options);
  }

  post(endpoint: string, body: any | null, options: any = { responseType: 'text' }): Observable<any> {
    return super.post(endpoint, body, options);
  }
}
