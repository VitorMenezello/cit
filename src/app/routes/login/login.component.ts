import {Component, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MockService} from '../../services/mock.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent {
  loading = false;
  loginForm: FormGroup;
  loginFormModel = {
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  };

  constructor(
    private mockService: MockService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.mockService.initialize();
    const usuario = this.mockService.currentUser.getValue();
    if (usuario) {
      this.router.navigate(['/lists/draft']).then();
    }
    this.loginForm = this.formBuilder.group(this.loginFormModel);
  }

  loginSubmit(event: any, form: FormGroup): void {
    event.stopPropagation();

    if (form.valid) {
      form.disable();
      this.loading = true;

      this.mockService.login(form.value)
        .then(() => {
          form.enable();
          this.loading = false;
        }, (error) => {
          console.log(error);
          form.enable();
          this.loading = false;
        });
    }
  }
}
