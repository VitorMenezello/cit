import {Component, ViewEncapsulation} from '@angular/core';
import {MockService} from '../../services/mock.service';
import {MatCheckboxChange} from '@angular/material';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent {
  loading = false;
  list = {
    totalItems: 0,
    pageSize: 5,
    items: []
  };
  selected: Array<number> = [];

  constructor(private mockService: MockService) {
    this.getListagem();
  }

  getListagem(): void {
    this.loading = true;
    this.mockService.getListagem().subscribe((result) => {
      // console.log(JSON.parse(result));
      // JSON inválido

      this.list = {
        totalItems: 100,
        pageSize: 5,
        items: [
          {
            id: 1,
            type: 'clean',
            last: 'Orange J.',
            needBy: '1552942603',
            po: '132401934',
            arrival: 'Yestarday, 11am',
            location: {
              id: 1,
              name: 'Belo Horizonte'
            }
          },
          {
            id: 2,
            type: 'clean',
            last: 'Orange J.',
            needBy: '1552942603',
            po: '132401934',
            arrival: 'Jan, 11h 10:15am',
            location: {
              id: 2,
              name: 'Campinas'
            }
          },
          {
            id: 3,
            type: 'clean',
            last: 'Orange J.',
            needBy: '1552942603',
            po: '132401934',
            arrival: '8am',
            location: {
              id: 1,
              name: 'Belo Horizonte'
            }
          },
          {
            id: 4,
            type: 'clean',
            last: 'Orange J.',
            needBy: '1552942603',
            po: '132401934',
            arrival: '7am',
            location: {
              id: 3,
              name: 'São Paulo'
            }
          },
          {
            id: 5,
            type: 'clean',
            last: 'Orange J.',
            needBy: '1552942603',
            po: '132401934',
            arrival: '7am',
            location: {
              id: 4,
              name: 'Contagem'
            }
          }
        ]
      };

      this.loading = false;
    });
  }

  toggle(id: number, event: MatCheckboxChange): void {
    if (event.checked) {
      this.selected.push(id);
    } else {
      const index = this.selected.indexOf(id);
      if (index >= 0) {
        this.selected.splice(index, 1);
      }
    }
  }

  exists(id: number): boolean {
    return this.selected.indexOf(id) > -1;
  }

  isChecked(): boolean {
    return this.selected.length === this.list.items.length;
  }

  isIndeterminate(): boolean {
    return this.selected.length > 0 && !this.isChecked();
  }

  toggleAll(event: MatCheckboxChange) {
    if (event.checked) {
      this.list.items.forEach((item: any) => {
        this.selected.push(item.id);
      });
    } else {
      this.selected = [];
    }
  }
}
