import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {MockService} from '../services/mock.service';
import {MatDialog} from '@angular/material';
import {LogoutDialogComponent} from '../components/dialog/logout-dialog/logout-dialog.component';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LayoutComponent implements OnInit, OnDestroy {
  user: string | null = null;

  private readonly unsubscribeAll: Subject<any>;

  constructor(private mockService: MockService, private dialog: MatDialog) {
    this.unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.mockService.currentUser.subscribe((user: string | null) => this.user = user);
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  logout(): void {
    this.dialog.open(LogoutDialogComponent, { data: { username: this.user } });
  }
}
