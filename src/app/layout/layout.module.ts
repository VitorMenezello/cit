import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatIconModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSidenavModule,
  MatButtonModule,
  MatExpansionModule,
  MatRippleModule,
} from '@angular/material';
import {RoutesModule} from '../routes/routes.module';
import {MockService} from '../services/mock.service';
import {LayoutComponent} from './layout.component';
import {LogoutDialogComponent} from '../components/dialog/logout-dialog/logout-dialog.component';

@NgModule({
  declarations: [
    LayoutComponent
  ],
  entryComponents: [
    LogoutDialogComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatButtonModule,
    RoutesModule,
    MatExpansionModule,
    MatRippleModule
  ],
  providers: [
    MockService
  ],
  exports: [
    LayoutComponent
  ]
})
export class LayoutModule {
}
