import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {ApiService} from '../api/api.service';

@Injectable()
export class MockService {
  public currentUser = new BehaviorSubject<string>('');

  constructor(private router: Router, private api: ApiService) {
    this.initialize();
  }

  initialize(): void {
    try {
      const user = localStorage.getItem('CIT.user');
      if (!user) {
        this.redirecionarParaLogin();
        return;
      }
      this.currentUser.next(user);
    } catch (error) {
      this.redirecionarParaLogin();
    }
  }

  login(params: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.api.post('http://www.mocky.io/v2/5e72225d3300004f0044c704', params)
        .toPromise()
        .then((result: any) => {
          // result: { message: "User authenticated", fullName: "Usuário padrão"}
          // console.log(JSON.parse(result));
          // JSON inválido
          result = {message: 'User authenticated', fullName: 'Usuário padrão'};
          console.log(result.fullName);

          localStorage.setItem('CIT.user', result.fullName);
          this.currentUser.next(result.fullName);

          resolve(result);
          this.router.navigate(['/lists/draft']);
        }, (error) => {
          reject(error);
        });
    });
  }

  logout(): void {
    localStorage.clear();
    this.currentUser.next(null);
    this.redirecionarParaLogin();
  }

  redirecionarParaLogin(): void {
    this.router.navigate(['/login']).then();
  }

  getListagem(): Observable<any> {
    return this.api.get('http://demo9757998.mockable.io/list');
  }
}
