import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatIconModule, MatButtonModule, MatDialogModule} from '@angular/material';
import {LogoutDialogComponent} from './logout-dialog/logout-dialog.component';

@NgModule({
  declarations: [
    LogoutDialogComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule
  ],
  exports: [
    LogoutDialogComponent
  ]
})
export class DialogModule {
}
